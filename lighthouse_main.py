import json
import os

import pandas as pd
import time
from numpy.lib.function_base import extract
from datetime import datetime
from os.path import join

from google.cloud import bigquery
client = bigquery.Client.from_service_account_json(r"")
project = client.project
dataset_id = 'AperBI'
dataset_ref = bigquery.DatasetReference(project, dataset_id)

getdate = datetime.now().strftime("%m-%d-%y")


URLS = {
# "https://tienda.iupp.com.ar/": "ITAUAR",
# "https://mall.icbc.com.ar": "ICBC",
# "https://www.tiendasupervielle.com": 'SPV',
# "https://www.tiendaclic.com.ar/": "CLIC",
# "https://shopping.smiles.com.ar/": "SMILES",
# "https://productos.quiero.com.ar/": "GALICIA",
# "https://www.simplebuy.cl/": "SCOTIA",
# "https://www.bicestore.cl/": "BICE",
# "http://tiendaindigo.com.mx/": "INVEX",
# "https://tiendanaranja.com.py/": "ITAUPY",
# "https://shop.bbva.com.ar/": "BBVA",
"https://tu360compras.grupobancolombia.com/": "BANCOL"
}

def extract_info(preset, url, store):
    source = 'desktop' if preset == 'desktop' else 'mobile'

    command = 'lighthouse --chrome-flags="--headless"--disable-storage-reset="true" --preset=' + preset + ' --output=json --output-path=' + './' + 'Report' + '_' + preset + getdate + '.report.json ' + url
    stream = os.popen(command)
    print(stream)

    time.sleep(180)
    json_filename = join('./', 'Report' + '_' + preset + getdate + '.report.json')
    print("opening ", json_filename)

    with open(json_filename, encoding="utf8") as json_data:
        loaded_json = json.load(json_data)
        # print(loaded_json)
        try:
            product_name = loaded_json["audits"]["largest-contentful-paint-element"]["details"]["items"][0]["node"]["nodeLabel"] ### get the name of the product to be descriptive
            fcps = str(round(loaded_json["audits"]["first-contentful-paint"]["score"] * 100))
            sis = str(round(loaded_json["audits"]["speed-index"]["score"] * 100))
            lcps = str(round(loaded_json["audits"]["largest-contentful-paint"]["score"] * 100))
            seo = str(round(loaded_json["categories"]["seo"]["score"] * 100))
            performance = str(round(loaded_json["categories"]["performance"]["score"] * 100))
            best_practices = str(round(loaded_json["categories"]["best-practices"]["score"] * 100))
            print("Se extrajeron los campos.")
        except Exception as e:
            print("Hubo un error ", str(e))
            product_name = fcps = sis = lcps = seo = performance = best_practices = '-'
        # data = [urls.index(url), product_name, url, fcps, sis, lcps, seo, performance, best_practices]
        data = {
            'product_name': [product_name],
            'url': [url],
            'fcps': [fcps],
            'sis': [sis],
            'lcps': [lcps],
            'seo': [seo],
            'performance': [performance],
            'best_practices': [best_practices],
            'source': [source],
            'datetime': [datetime.utcnow()],
            'store_name': store
        }
        df_results = pd.DataFrame(data)
        print(df_results)
        return df_results


def main():

    lighthouse_df = pd.DataFrame()
    for url, store in URLS.items():
        df_mobile = extract_info(preset='perf', url=url, store=store) ### run the test on mobile
        df_desktop = extract_info(preset='desktop', url=url, store=store) ### run the test on desktop
        lighthouse_df = pd.concat([lighthouse_df, df_mobile, df_desktop])

        job_config = bigquery.LoadJobConfig(
            schema=[
                bigquery.SchemaField("datetime", bigquery.enums.SqlTypeNames.TIMESTAMP)],
            write_disposition="WRITE_APPEND"
        )
        table = 'AperBI.lighthouse_data'
        try:
            job = client.load_table_from_dataframe(lighthouse_df, table, job_config=job_config)
            print(job)
            print(len(lighthouse_df), "elements saved to table ", table)
        except Exception as e:
            print(e)

main()
print("Finish")